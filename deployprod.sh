#! bin/sh

. ./config.sh

ROLLBACK_VERSION=

#met le site en maintenance (lien symbolique prod vers maintenance)
enable_maintenance()
{
    echo "> mise en maintenance du site"
    ln -sfn $APP_PATH/maintenance $APP_PATH/prod
}

#rsync fichier de preprod dans fichier version
create_prod_version()
{
    local VERSION_PATH=`readlink -f "$ACTIVE_DIRECTORY/preprod"`
    VERSION=`basename "$VERSION_PATH"`
    echo "> rsync --recursive --exclude-from=$FILE $APP_PATH/preprod/* $APP_PATH/version/$VERSION/"
    rsync --verbose --recursive --exclude-from=$FILE $APP_PATH/preprod/* $APP_PATH/version/$VERSION/

    echo "> Création du lien symbolique $APP_PATH/version/$VERSION $APP_PATH/prod"
    ln -sfn $APP_PATH/version/$VERSION $APP_PATH/prod

    echo "> Modification des droits pour la version active"
    chown "$USER:$GROUP" -R "$APP_PATH/prod"
}

find_previous_version()
{
    local CURRENT_VERSION_PATH=`readlink -f "$APP_PATH/prod"`
    local CURRENT_VERSION=`basename "$CURRENT_VERSION_PATH"`

    for CANDIDATE_VERSION in `ls -r $APP_PATH/version`
    do
        if [ "$CURRENT_VERSION" -gt "$CANDIDATE_VERSION" ]
        then
            if [ ! "$ROLLBACK_VERSION" ] || [ "$CANDIDATE_VERSION" -gt "$ROLLBACK_VERSION" ]
            then
                ROLLBACK_VERSION=$CANDIDATE_VERSION
                break
            fi
        fi
    done
}

rollback()
{
    echo "$ROLLBACK_VERSION" | grep -E ^[0-9]+$
    if ! [ $? -eq 0 ]
    then
        ROLLBACK_VERSION=
        find_previous_version
    fi
    
    if [ ! "$ROLLBACK_VERSION" ]
    then
        echo "> Version précédente non trouvée, rollback impossible"
        exit 3
    fi
    
    if [ -L "$APP_PATH/prod" ]
    then
        echo "> Suppression de l'ancien lien symbolique"
        rm "$APP_PATH/prod"
    fi

    local CURRENT_VERSION_PATH=`readlink -f "$APP_PATH/version/$ROLLBACK_VERSION"`
    echo $CURRENT_VERSION_PATH
    echo "> Création du nouveau lien symbolique vers version/$ROLLBACK_VERSION"
    ln -sfn "$CURRENT_VERSION_PATH" "$APP_PATH/prod"

    echo "> Modification des droits pour la version active"
    chown "$USER:$GROUP" -R "$APP_PATH/prod"
}

prompt_help()
{
    echo "
Activer la page de maintenance :
-m|--maintenance
Déployer la version de preprod :
-d|--deploy
Effectuer un rollback (par défaut version précédente) :
-r|--rollback [VERSION]
Afficher l'aide :
-h|--help
Toutes les options peuvent être utilisées mais sans concaténation :
    sh deployprod.sh -d -r  > possible
    sh deployprod.sh -dr    > impossible
Si plusieurs options sont utilisées en même temps l'ordre d'éxecution est
toujours le suivant :
1) Activation de la page de maintenance
2) Deploiement de la nouvelle version
3) Rollback"

}
#enable_maintenance
#create_prod_version
#find_previous_version
#rollback

MAINTENANCE=false
DEPLOY=false
ROLLBACK=false

while [ $# -gt 0 ]
do
    case "$1" in
        -d|--deploy)
            DEPLOY=true
            shift
        ;;
        -r|--rollback)
            ROLLBACK_VERSION=$2
            ROLLBACK=true
            shift
        ;;
        -h|--help)
            prompt_help
            shift
        ;;
        -m|--maintenance)
            MAINTENANCE=true
            shift
        ;;
        (-*|--*)
            echo "$0: Erreur option non reconnu $1 (-h pour l'aide)" 1>&2
            exit 3
        ;;
        (*)
            shift;;
    esac
done

if ($MAINTENANCE)
then
    enable_maintenance
fi

if ($DEPLOY)
then
    create_prod_version
fi

if ($ROLLBACK)
then
    rollback
fi

