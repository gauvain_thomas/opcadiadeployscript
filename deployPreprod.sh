#! /bin/sh

. ./config.sh

sourcesCreate()
{
    echo "> Create folders and clone sources "


    #mkdir "preprod" /* Si dossier existant, crée le lien à l'intérieur du dossier */
    mkdir "preprodVersions"


    #Download sources
    echo "git clone git@bitbucket.org:"$GIT_NAME"/"$GIT_REPO".git"
    #git clone git@bitbucket.org:"$GIT_NAME"/"$GIT_REPO".git
    git clone https://"$GIT_NAME"@bitbucket.org/"$GIT_NAME"/"$GIT_REPO".git $ACTIVE_DIRECTORY/preprodVersions/$DATE
    test "unable to clone the repository"

    #mkdir "$GIT_REPO" /* Le dossier est créer par git clone */



    #Create Symbolic link
    ln -snf "$ACTIVE_DIRECTORY/preprodVersions/$DATE" "$ACTIVE_DIRECTORY/preprod"
    
    echo "> Creation du dossier de cache"
    if [ ! -d "$ACTIVE_DIRECTORY/preprod/app/cache" ]
    then
        mkdir "$ACTIVE_DIRECTORY/preprod/app/cache"
        test "Impossible de créer le dossier de cache"
    fi

    echo "> Creation du dossier de logs"
    if [ ! -d "$ACTIVE_DIRECTORY/preprod/app/logs" ]
    then
        mkdir "$ACTIVE_DIRECTORY/preprod/app/logs"
        test "Impossible de créer le dossier de logs"
    fi

    echo "> Modification des droits pour la version active"
    chown "$USER:$GROUP" -R "$ACTIVE_DIRECTORY/preprod"
    #cache et log chmod 770
    #sur reste 650
}

sourcesUpdate()
{
    echo "> Update the latest sources from the repository"

    cd "$ACTIVE_DIRECTORY/preprod"

    #Update sources
    git pull
}

vendorsInstall()
{
    echo "> Install Composer"

    cd "$ACTIVE_DIRECTORY/preprod"

    #Install Vendors
    php composer.phar install
}

# permissons(){
# }

cacheWarmUp()
{
    echo "> Warm up Cache for prod env"

    cd "$ACTIVE_DIRECTORY/preprod"

    #Cache Warm_up
    php app/console cache:warmup --env=prod
}

optimizeAutoload()
{
    echo "> Optimize Autoload"
    
    cd "$ACTIVE_DIRECTORY/preprod"
    
    php composer.phar dump-autoload --optimize
}

error()
{
    if [ "$1" ]
    then
        echo "$1" 1>&2
    fi

    exit 1
}

test()
{
    if [ $? != 0 ]
    then
        if [ "$1" ]
        then
            error "$1"
        else
            error
        fi
    fi
}

find_previous_version()
{
    local CURRENT_VERSION_PATH=`readlink -f "$ACTIVE_DIRECTORY/preprod"`
    local CURRENT_VERSION=`basename "$CURRENT_VERSION_PATH"`

    for CANDIDATE_VERSION in `ls -r $ACTIVE_DIRECTORY/version`
    do
        if [ "$CURRENT_VERSION" -gt "$CANDIDATE_VERSION" ]
        then
            if [ ! "$ROLLBACK_VERSION" ] || [ "$CANDIDATE_VERSION" -gt "$ROLLBACK_VERSION" ]
            then
                ROLLBACK_VERSION=$CANDIDATE_VERSION
                break
            fi
        fi
    done
}

rollback()
{
    echo "$ROLLBACK_VERSION" | grep -E ^[0-9]+$
    if ! [ $? -eq 0 ]
    then
        ROLLBACK_VERSION=
        find_previous_version
    fi
    
    if [ ! "$ROLLBACK_VERSION" ]
    then
        echo "> Version précédente non trouvée, rollback impossible"
        exit 3
    fi

    local CURRENT_VERSION_PATH=`readlink -f "$ACTIVE_DIRECTORY/preprodVersions/$ROLLBACK_VERSION"`
    echo $CURRENT_VERSION_PATH
    echo "> Création du nouveau lien symbolique vers version/$ROLLBACK_VERSION"
    ln -sfn "$CURRENT_VERSION_PATH" "$ACTIVE_DIRECTORY/preprod"

    echo "> Modification des droits pour la version active"
    chown "$USER:$GROUP" -R "$ACTIVE_DIRECTORY/preprod"
}

prompt_help()
{
    echo "
Importer une nouvelle version depuis le dépot bitbucket :
-c|--create
Mettre à jour la version courante :
-u|--update
Effectuer un rollback (par défaut vers la version précédente)
-r|--rollback [VERSION]
Installer les vendors :
-v|--vendors
Préchauffer le cache :
-w|--cachewarmup
Optimiser l'autoloader :
-o|--optimize
Afficher l'aide :
-h|--help
    "
}

while [ $# -gt 0 ]
do
    case "$1" in
        -c|--create)
            sourcesCreate
            shift
        ;;
        -r|--rollback)
            ROLLBACK_VERSION=$2
            rollback
            shift
        ;;
        -u|--update)
            sourcesUpdate
            shift
        ;;
        -w|--cachewarmup)
            cacheWarmUp
            shift
        ;;
        -v|--vendors)
            vendorsInstall
            shift
        ;;
        -h|--help)
            prompt_help
            shift
        ;;
        -o|--optimize)
            optimizeAutoload
            shift
        ;;
        (-*|--*)
            echo "$0: Erreur option non reconnu $1 (-h pour l'aide)" 1>&2
            exit 3
        ;;
        (*)
            shift;;
    esac
done



